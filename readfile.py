### Obtener puntuaciones altas
def gethighScores():
    #leemos el archivo
    scoreFile=open('highscore.txt','r+')
    scoreList = getHighScore_aux(scoreFile)
    scoreFile.close()
    return scoreList
    
def getHighScore_aux(file):
    line = file.readline()
    if(line==""):
        return []
    else:
        line = line.replace("\n","")
        (name,points) = line.split(",")
        if(points.isdigit()):
            points = int(points)
        else:
            points = -1
        return [(name,points)]+getHighScore_aux(file)

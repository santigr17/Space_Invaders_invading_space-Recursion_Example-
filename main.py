"""          Invaders: give me some Space
                 Spiced with Python
"""
#BIBLIOTECAS
from tkinter import *
from tkinter import messagebox
import time
from threading import Thread
import random
import threading
import os

#Funcion cargar imagen
def cargarImg(nombre):
    ruta=os.path.join('img',nombre)
    imagen=PhotoImage(file=ruta)
    return imagen


### Obtener puntuaciones altas
def gethighScores():
    #leemos el archivo
    scoreFile=open('highscore.txt','r+')
    scoreList = getHighScore_aux(scoreFile)
    scoreFile.close()
    return scoreList
    
def getHighScore_aux(file):
    line = file.readline()
    if(line==""):
        return []
    else:
        line = line.replace("\n","")
        (name,points) = line.split(",")
        if(points.isdigit()):
            points = int(points)
        else:
            points = -1
        return [(name,points)]+getHighScore_aux(file)

#____ventana principal____#
root=Tk()
root.title('SPACE IVADERS')
root.minsize(800,600)
root.resizable(width=NO,height=NO)

#Canvas de root
C_root=Canvas(width=800,height=600, bg='black')
C_root.place(x=0,y=0)

#Fondo de la pantalla principal
fondoImg=cargarImg('fondo0.gif')
F_root=Label(C_root,width=802,height=602,image=fondoImg,bg='black',cursor='mouse')
F_root.place(x=-1,y=-1)

#titulo
L_nombre=Label(C_root,text='NOMBRE JUGADOR:',font=('Agency FB',28), bg='Black',fg='light green')
L_nombre.place(x=150,y=300)

#Entrada del nombre
nombre=Entry(C_root,width=10,font=('Agency FB',28),bg='black',fg='light green')
nombre.place(x=450,y=300) 


def play():
        mensaje=str(nombre.get())
        if mensaje != '' and len(mensaje)<15:
            player=mensaje
            if messagebox.askyesno('Confirme su nombre',('Esta seguro que "'+mensaje+'" es el nombre que quiere utilizar?'))==True:
                root.withdraw()
                return W_game(player)
        else:
            messagebox.showerror('Error en el nombre','El nombre de jugador no debe ser vacío o mayor a 15 caracteres')

def pressPlay(event):
    play()
root.bind('<Return>',pressPlay)

#____Ventana ABOUT____#
def W_about(): 
    root.withdraw()
    about=Toplevel()
    about.title('CREDITOS')
    about.minsize(600,400)
    about.resizable(width=NO, height=NO)

    #Canvas del about
    C_about=Canvas(about,width=601,height=401,bg='#D7D7D7')
    C_about.place(x=0,y=0)
    

    #Fondo del about
    infoIma=cargarImg('fondo1.gif')
    F_about=Label(about,width=601,height=401,image=infoIma,bg='#D7D7D7')
    F_about.photo=infoIma
    F_about.place(x=0,y=0)

    #Informacion del about
    T_about=Label(about,text='Instituto Tecnologico de Costa Rica\nComputer Engineering\Milton Villegas Lemus\nSantiago Gamboa Ramírez\n2014092362\nVersion: 4.1.0\nfecha de emision:\n25/04/2014\n',font=('Agency FB',14),bg='#D7D7D7',fg='white')
    T_about.place(x=45,y=20)

    #Foto personal
    Foto=cargarImg('foto.gif')
    L_foto=Label(about,width=120,height=120, image=Foto,bg='#D7D7D7')
    L_foto.photo=Foto
    L_foto.place(x=10,y=200)
    
    #Funcion volver al root
    def back():
        about.destroy()
        root.deiconify()
        return SONG1()

    #boton de regresar
    B_root=Button(about,text='BACK',command=back,bg='black',fg='white')
    B_root.place(x=500,y=280)

#____Ventana de altos puntajes_____#
def W_score():
    root.withdraw()
    score=Toplevel()
    score.title('BEST SCORE')
    score.minsize(800,600)
    score.resizable(width=NO,height=NO)

    #Canvas del los altos puntajes
    C_score=Canvas(score,width=800,height=600,bg='black')
    C_score.place(x=-1,y=-1)

    #Imagen de fondo
    fondoImg=cargarImg('fondo0.gif')
    F_score=Label(C_score,width=800,height=600,image=fondoImg,bg='black')
    F_score.place(x=-1,y=-1)
    F_score.photo=fondoImg

    
    def printScores():
        myScores = gethighScores()
        if(len(myScores)>0):
            titleName = Label(score,text="Jugador",font=('Agency FB',28),bg='black',fg='light green')
            titleName.place(x=300,y=100)
            titlePoints=Label(score,text="Puntos",font=('Agency FB',28),bg='black',fg='light green')
            titlePoints.place(x=500,y=100)
            printScores_aux(myScores,0)
            

    def printScores_aux(listScores,i):
        #Informacion del about
        if(i < len(listScores)):
            name=Label(score,text=listScores[i][0],font=('Agency FB',24),bg='black',fg='light green')
            name.place(x=300,y=200+50*i)
            points=Label(score,text=listScores[i][1],font=('Agency FB',24),bg='black',fg='light green')
            points.place(x=500,y=200+50*i)
            return printScores_aux(listScores,i+1)
    printScores()
    
    #Funcion volver al root
    def back1():
        score.destroy()
        root.deiconify()
        
    #boton de regresar
    B_root=Button(F_score,text='BACK',command=back1,bg='black',fg='light blue')
    B_root.place(x=150,y=150)



#_____Ventana del juego_______#
def W_game(name):
    game=Toplevel()
    game.minsize(width=800,height=600)
    game.resizable(width=NO,height=NO)

    #Canvas del juego
    C_game=Canvas(game,width=800,height=600 ,bg='black',)
    C_game.place(x=0,y=0)
##
##    #Musica de la ventana
##    def SONG3():
##        reproducir='cancion3.wav'
##        winsound.PlaySound(reproducir, winsound.SND_ASYNC)
##            
##    #hilo de musica
##    b=Thread(target=SONG3,args=())
##    b.start()

##    #Musica de la ventana
##    def SONG4():
##        winsound.PlaySound(None, winsound.SND_ASYNC)
            
   

    #Canvas del puntaje
    ImgScore=cargarImg('fondo4.gif')
    F_score=Label(C_game,width=822,height=60,image=ImgScore,bg='black')
    F_score.place(x=-1,y=550)
    F_score.photo=ImgScore

    #Nombre del jugador
    N_player=Label(C_game, text=('Player: '+name),font=('Agency FB',16),fg='white',bg='#073863')
    N_player.place(x=100,y=565)

    #Nave q destruye a los invasores
    ImgShip=cargarImg('nave2.gif')
    Ship=C_game.create_image(45,500,image=ImgShip)

    #Funcion de restart
    def restart():
        game.destroy()
        return W_game(name)


    global endgame
    endgame = False
    
    #Boton de restart
    B_restart=Button(C_game,text='RESTART',command=restart,bg=('#073863'),font=('Agency FB',11),fg='white')
    B_restart.place(x=650,y=565)

    #funcion de back
    def back():
        game.destroy()
        root.deiconify()

    #Boton de volver
    B_back=Button(C_game,text='BACK',command=back,bg=('#073863'),font=('Agency FB',11),fg='white')
    B_back.place(x=720,y=565)

    #Creamos a los invasores
    enemyIMG=cargarImg('nave1.gif')
    xInicial = 45
    enemyList = []
    def setEnemies(i,x,y):
        if(i==12):
            x = xInicial
            y+=50
        elif(i==24):
            x = xInicial
            y+=50
        enemyList.append(C_game.create_image(x,y,image=enemyIMG))
        if(i<35):
            return setEnemies(i+1,x+40,y)
        else:
            return

    setEnemies(0,xInicial,25)
    ImgShip=cargarImg('nave2.gif')
    Ship=C_game.create_image(45,525,image=ImgShip)

    #movimiento de la nave
    def right(event):
        global endgame
        if (not endgame and C_game.coords(Ship)[0] <=725):
            C_game.move(Ship,20,0)
    def left(event):
        global endgame
        if (not endgame and C_game.coords(Ship)[0] >=75):
            C_game.move(Ship,-20,0)

    game.bind('<Right>',right)
    game.bind('<Left>',left)


    global points
    points = 0
    
    
    ###### Disparo enemigo
    global bbullet_flag
    bbullet_flag = False
    imgBadBullet=cargarImg('badbullet.gif')
    def badBullet(x, y, canvas, ship):
        global bbullet_flag
        bbullet_flag = True
        y+=10
        bullet=canvas.create_image(x,y,image=imgBadBullet)
        canvas.after(1)
        canvas.update()
        badBullet_aux(bullet, canvas, ship)

    def badBullet_aux(bullet, canvas, ship):
        global bbullet_flag
        canvas.move(bullet,0,10)
        (x,y) = canvas.coords(ship)
        (bx,by) = canvas.coords(bullet)
        vert = bx >= x and bx <= x+20
        horiz = by <= y and by >= y-20
        if(vert and horiz):
            global endgame
            endgame = True
            canvas.delete(ship)
            canvas.delete(bullet)
            canvas.after(1)
            canvas.update()
            return
        
        elif(by>600):
            canvas.delete(bullet)
            canvas.after(1)
            canvas.update()
            bbullet_flag = False
            return
        else:
            canvas.after(1)
            canvas.update()
            time.sleep(0.05)
            return badBullet_aux(bullet, canvas, ship)
        
        
    ###### Movimiento de los enemigos
    firstX = 45
    vel = 0.2
    
    def moveEnemy(enemyList, ship, canvas, vel):
        if(not endgame):
            time.sleep(1)
            movedown_aux(0, 0, enemyList, ship, canvas, False, vel)
            
    def movedown_aux(i, j, enemies, ship, canvas, izq, vel):
        if(i<14):
            x = 20
            if(izq): x = -20 
            move_aux(enemies,canvas,0,x,0,ship)
            canvas.after(10)
            canvas.update()
            time.sleep(vel)
            return movedown_aux(i+1, j, enemies, ship, canvas, izq, vel)

        else:
            if(j<18):
                izq = not izq
                move_aux(enemyList, canvas, 0, 0, 20, ship)
                canvas.after(10)
                canvas.update()
                time.sleep(vel)
                vel-=0.01
                return movedown_aux(0, j+1, enemies, ship, canvas, izq, vel)
            else:
               return
            
    def move_aux(enemies, canvas, i, x, y, ship):
        if( not endgame and i<len(enemies)):
            canvas.move(enemies[i],x,y)
            ####### Crear una bala de enemigos
            global bbullet_flag
            ran = random.randrange(0,len(enemyList))
            if(ran == i and not bbullet_flag):
                (xEnemy,yEnemy) = canvas.coords(enemies[i])
                bb=Thread(target=badBullet,args=(xEnemy, yEnemy, canvas, ship))
                bb.start() 
            return move_aux(enemies, canvas, i+1, x, y, ship)
        else:
            return
        
    
    m=Thread(target=moveEnemy,args=(enemyList, Ship, C_game, vel))
    m.start()


    ###### Disparo de la nave 
    imgBullet=cargarImg('bala.gif')
    global shooting
    shooting = False
        
    def crash_aux(cords, enemies, canvas, hit):
        if(hit[0] >= len(enemies)):
            hit[1] = False
            return
        else:
            (x,y) = canvas.coords(enemies[hit[0]])
            vert = cords[0] >= x and cords[0] <= x+20
            horiz = cords[1] <= y and cords[1] >= y-20
            if(vert and horiz):
                hit[1] = True
                return
            else:
                hit[0] += 1
                return crash_aux(cords, enemies, canvas, hit)
        
    def shoot(x, y, enemies, canvas):
        y += 50 
        bullet=C_game.create_image(x,y,image=imgBullet)
        canvas.after(1)
        canvas.update()
        shoot_aux(bullet, enemies, canvas, x, y)
        global shooting
        shooting = False

    def shoot_aux(bullet, enemies, canvas, x, y):
        y -= 20
        canvas.move(bullet, 0, -20)
        hit = [0, False]
        time
        crash_aux((x,y), enemies, canvas, hit) 
        if(hit[1]):
            global points
            points += 100
            destroy = enemies[hit[0]]
            canvas.delete(destroy)
            enemies.remove(destroy)
            canvas.delete(bullet)
            canvas.after(1)
            canvas.update()
            return
        elif(y<=0):
            canvas.delete(bullet)
            canvas.after(1)
            canvas.update()
            return 
        else:
            canvas.after(1)
            canvas.update()
            time.sleep(0.05)
            return shoot_aux(bullet, enemies, canvas, x, y)
            

    def spaceKey(event):
        global shooting
        if(not endgame and not shooting):
            shooting = True
            xy = C_game.coords(Ship)
            b=Thread(target=shoot,args=(xy[0], xy[1], enemyList, C_game))
            b.start()
        
    game.bind('<space>',spaceKey)
    game.mainloop()




#Botones ventana principal
B_about=Button(F_root, text='ABOUT', command=W_about,font=('Agency FB',16),bg='black',fg='light green',cursor='mouse')
B_about.place(x=250,y=550)

B_score=Button(F_root,text='HIGH SCORES',command=W_score,font=('Agency FB',16),bg='black',fg='light green',cursor='mouse')
B_score.place(x=425,y=550)

B_game=Button(F_root,text='GAME',command=play,font=('Agency FB' ,16),bg='green',fg='black',cursor='mouse')
B_game.place(x=600,y=550)

B_exit=Button(F_root,text='EXIT',command=exit,font=('Agency FB',16),bg='black',fg='light green',cursor='mouse')
B_exit.place(x=100,y=550)

root.mainloop()

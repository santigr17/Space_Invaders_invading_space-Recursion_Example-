# Space Invanders invading space
Instituto Tecnológico de Costa Rica  
Computer Engineering  
Introduction to programming and Programming workshop  
2018

### Prerequisites
To run this project you will need:
1. Python 3.6  

The libraries used are:
1. Tkinter
2. Messagebox
3. Threading
4. os
5. Time

## Getting Started
This is an example of how to use recursion in python to build a full functional project.  
The idea is that you learn how to use recursion.  
We know that this can be less efficient than loops like while or for.  
However, this is an academic project.

### Known problems
This project does not contain music, in order to maintain it lighter

## Author

* Santiago Gamboa Ramírez  
santigr17@gmail.com  
2014092362  

### Version
1.0.0

